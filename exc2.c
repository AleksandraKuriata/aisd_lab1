#include<stdio.h>
#include<stdlib.h>
#include<time.h>

typedef struct listEl {
    int value;
    struct listEl *next;
} listEl;

typedef struct sentinel {
    listEl *head, *tail;
    int counter;
} sentinel;

void insertByID(sentinel **container, int IDToInsert, int valueToInsert) {
    listEl *newEl = (listEl *) malloc(sizeof(listEl));
    newEl->value = valueToInsert;

    if (IDToInsert == 0) {
        if ((*container)->head == NULL) {
            newEl->next = NULL;
            (*container)->head = newEl;
            (*container)->tail = newEl;
            (*container)->counter++;
        }
        else {
            newEl->next = (*container)->head;
            (*container)->head = newEl;
            (*container)->counter++;
        }
    }
    else {
        if (IDToInsert < 0 || IDToInsert > (*container)->counter) {
            printf("Unable to insert value at this elementID!\n");
            return;
        }
        else if (IDToInsert == (*container)->counter) {
            newEl->next = NULL;
            (*container)->tail->next = newEl;
            (*container)->tail = newEl;
        }
        else {
            listEl *tempEl = (*container)->head;
            listEl *oldTempEl;
            while (IDToInsert != 0) {
                oldTempEl = tempEl;
                tempEl = tempEl->next;
                IDToInsert--;
            }
            newEl->next = tempEl;
            oldTempEl->next = newEl;
            (*container)->counter++;
        }
    }
    return;
}

int removeByID(sentinel **container, int IDToRemove) {
    int result = 0;

    if (IDToRemove == 0) {
        if ((*container)->head == NULL) {
            printf("Unable to delete element with this ID, list is empty, -1 returned!\n");
            return -1;
        }
        else if ((*container)->head->next == NULL) {
            result = (*container)->head->value;
            free((*container)->head);
            (*container)->head = NULL;
            (*container)->tail = NULL;
        }
        else {
            result = (*container)->head->value;
            listEl *tempEl = (*container)->head;
            (*container)->head = tempEl->next;
            free(tempEl);
        }
    }
    else {
        if (IDToRemove < 0 || IDToRemove > ((*container)->counter - 1)) {
            printf("Unable to delete element with this ID, -1 returned!\n");
            return -1;
        }
        listEl *tempEl = (*container)->head;
        listEl *oldTempEl = (*container)->head;
        while (IDToRemove != 0) {
            oldTempEl = tempEl;
            tempEl = tempEl->next;
            IDToRemove--;
        }
        result = tempEl->value;
        oldTempEl->next = tempEl->next;
        if (tempEl->next == NULL) (*container)->tail = oldTempEl;
        free(tempEl);
    }
    (*container)->counter--;
    return result;
}

listEl *searchByID(sentinel **container, int IDToSerach) {
    if (IDToSerach < 0 || IDToSerach > (*container)->counter - 1) {
        printf("No value of that ID found!\n");
        return NULL;
    }
    else {
        listEl *temp = (*container)->head;
        while (IDToSerach != 0) {
            temp = temp->next;
            IDToSerach--;
        }
        return temp;
    }
}

int size(sentinel **container) {
    printf("Size queue : %d\n", (*container)->counter);
    return (*container)->counter;
}

void merge(sentinel **container1, sentinel **container2) {
    (*container1)->tail->next = (*container2)->head;
    (*container1)->tail = (*container2)->tail;
    (*container1)->counter += (*container2)->counter;
    (*container2)->head = NULL;
    (*container2)->tail = NULL;
    free(*container2);
}

int main() {
    sentinel *container = (sentinel *) malloc(sizeof(sentinel));
    container->head = NULL;
    container->tail = NULL;
    container->counter = 0;

    double average = 0;

     for(int i = 0; i < 1000; i++){
       insertByID(&container, 0, i + 3);
     }

     for(int i = 0; i < 1000000; i++){
       clock_t timeTemp = clock();
       searchByID(&container, rand()%1000);
       average += (clock() - timeTemp);
     }
     printf("%lf\n", average/1000000);

     while(container->head != NULL){
       removeByID(&container, 0);
     }
    free(container);
    return 0;
}

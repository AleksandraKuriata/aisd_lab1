#include<stdio.h>
#include<stdlib.h>

typedef struct queue {
    int value;
    struct queue *next;
} queue;

typedef struct sentinel {
    queue *head, *tail;
    int counter;
} sentinel;

void push_back(sentinel **container, int newValue) {
    queue *new = (queue *) malloc(sizeof(queue));
    new->value = newValue;
    new->next = NULL;

    if ((*container)->head == NULL) {
        (*container)->head = new;
        (*container)->tail = new;
    }
    else {
        (*container)->tail->next = new;
        (*container)->tail = new;
    }
    printf("Add new value : %d\n", new->value);
    (*container)->counter++;
}

int pop_front(sentinel **container) {
    int result;

    if ((*container)->head == NULL) {
        printf("No more elements in queue, -1 returned!\n");
        return -1;
    }
    else if ((*container)->head->next == NULL) {
        result = (*container)->head->value;
        free((*container)->head);
        (*container)->head = NULL;
        (*container)->tail = NULL;
    }
    else {
        result = (*container)->head->value;
        queue *temp = (*container)->head;
        (*container)->head = temp->next;
        free(temp);
    }

    (*container)->counter--;
    printf("Delete value : %d\n", result);
    return result;
}

int size(sentinel **container) {
    printf("Size queue : %d\n", (*container)->counter);
    return (*container)->counter;
}

int main() {
    sentinel *container = (sentinel *) malloc(sizeof(sentinel));
    container->head = NULL;
    container->tail = NULL;
    container->counter = 0;

    push_back(&container, 5);
    push_back(&container, 6);
    push_back(&container, 7);

    pop_front(&container);
    pop_front(&container);
    pop_front(&container);
    pop_front(&container);

    size(&container);

    free(container);
    return 0;
}

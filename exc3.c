#include<stdio.h>
#include<stdlib.h>
#include<time.h>

typedef struct listEl{
  int value;
  struct listEl *next;
  struct listEl *prev;
} listEl;

typedef struct sentinel{
  listEl *head, *tail;
  int counter;
} sentinel;

void insertByID(sentinel **container, int IDToInsert, int valueToInsert){
  listEl *newEl = (listEl *)malloc(sizeof(listEl));
  newEl->value = valueToInsert;

  if(IDToInsert == 0){
    if((*container)->head == NULL){
      newEl->next = newEl;
      newEl->prev = newEl;
      (*container)->head = newEl;
      (*container)->tail = newEl;
      (*container)->counter++;
    }
    else{
      (*container)->head->prev = newEl;
      (*container)->tail->next = newEl;
      newEl->next = (*container)->head;
      newEl->prev = (*container)->tail;
      (*container)->head = newEl;
      (*container)->counter++;
    }
  }
  else if(IDToInsert < ((*container)->counter)/2){
    if(IDToInsert < 0){
      printf("Unable to insert value at this elementID!\n");
      return;
    }
    listEl *tempEl = (*container)->head;
    while(IDToInsert != 0){
      tempEl = tempEl->next;
      IDToInsert--;
    }

    newEl->prev = tempEl->prev;
    tempEl->prev->next = newEl;
    tempEl->prev = newEl;
    newEl->next = tempEl;
    (*container)->counter++;
  }
  else{
    IDToInsert = (*container)->counter - IDToInsert - 1;

    if(IDToInsert < -1){
      printf("Unable to insert value at this elementID!\n");
      return;
    }
    else if(IDToInsert == -1){
      (*container)->head->prev = newEl;
      (*container)->tail->next = newEl;
      newEl->next = (*container)->head;
      newEl->prev = (*container)->tail;
      (*container)->tail = newEl;
      (*container)->counter++;
    }
    else{
      listEl *tempEl = (*container)->tail;
      while(IDToInsert != 0){
        tempEl = tempEl->prev;
        IDToInsert--;
      }
      newEl->prev = tempEl->prev;
      tempEl->prev->next = newEl;
      tempEl->prev = newEl;
      newEl->next = tempEl;
      (*container)->counter++;
    }
  }
  return;
}

int removeByID(sentinel **container, int IDToRemove){
  int result = 0;

  if(IDToRemove == 0){
    if((*container)->head == NULL){
      printf("Unable to delete element with this ID, list is empty, -1 returned!\n");
      return -1;
    }
    else if((*container)->head->next == (*container)->head){
      result = (*container)->head->value;
      free((*container)->head);
      (*container)->head = NULL;
      (*container)->tail = NULL;
    }
    else{
      result = (*container)->head->value;
      listEl *tempEl = (*container)->head;
      (*container)->tail->next = tempEl->next;
      tempEl->next->prev = (*container)->tail;
      (*container)->head = tempEl->next;
      free(tempEl);
    }
  }
  else if(IDToRemove < ((*container)->counter)/2){
    if(IDToRemove < 0){
      printf("Unable to delete element with this ID, -1 returned!\n");
      return -1;
    }
    listEl *tempEl = (*container)->head;
    while(IDToRemove != 0){
      tempEl = tempEl->next;
      IDToRemove--;
    }
    result = tempEl->value;
    tempEl->next->prev = tempEl->prev;
    tempEl->prev->next = tempEl->next;
    free(tempEl);
  }
  else{
    IDToRemove = ((*container)->counter) - IDToRemove - 1;
    if(IDToRemove < 0){
      printf("Unable to delete element with this ID, -1 returned!\n");
      return -1;
    }
    else if(IDToRemove == 0){
      if((*container)->tail->prev == (*container)->tail){
        result = (*container)->tail->value;
        free((*container)->tail);
        (*container)->head = NULL;
        (*container)->tail = NULL;
      }
      else{
        result = (*container)->tail->value;
        listEl *tempEl = (*container)->tail;
        (*container)->head->prev = tempEl->prev;
        tempEl->prev->next = (*container)->head;
        (*container)->tail = tempEl->prev;
        free(tempEl);
      }
    }
    else {
      listEl *tempEl = (*container)->tail;
      while(IDToRemove != 0){
        tempEl = tempEl->prev;
        IDToRemove--;
      }
      result = tempEl->value;
      tempEl->next->prev = tempEl->prev;
      tempEl->prev->next = tempEl->next;
      free(tempEl);
    }
  }

  (*container)->counter--;
  return result;
}

listEl* searchByID(sentinel **container, int IDToSerach){
  if(IDToSerach > (*container)->counter - 1){
    printf("No value of that ID found!\n");
    return NULL;
  }
  if(IDToSerach < ((*container)->counter)/2){
    listEl *temp = (*container)->head;
    while(IDToSerach != 0){
      if(temp->next != (*container)->head) temp = temp->next;
      IDToSerach--;
    }
    return temp;
  }
  else{
    IDToSerach = (*container)->counter - IDToSerach - 1;
    listEl *temp = (*container)->tail;
    while(IDToSerach != 0){
      if(temp->prev != (*container)->tail) temp = temp->prev;
      IDToSerach--;
    }
    return temp;
  }
}

void merge(sentinel **container1, sentinel **container2){
  (*container1)->tail->next = (*container2)->head;
  (*container2)->head->prev = (*container1)->tail;
  (*container1)->head->prev = (*container2)->tail;
  (*container2)->tail->next = (*container1)->head;
  (*container1)->tail = (*container2)->tail;
  (*container1)->counter += (*container2)->counter;
  (*container2)->head = NULL;
  (*container2)->tail = NULL;
  free(*container2);
}

int size(sentinel **container){
  return (*container)->counter;
}

int main(){
    sentinel *container = (sentinel *)malloc(sizeof(sentinel));
    container->head = NULL;
    container->tail = NULL;
    container->counter = 0;

    /*sentinel *container2 = (sentinel *)malloc(sizeof(sentinel));
    container2->head = NULL;
    container2->tail = NULL;
    container2->counter = 0;*/

    double average = 0;
    for(int i = 0; i < 1000; i++){
        insertByID(&container, 0, i + 3);
    }
    for(int i = 0; i < 1000000; i++){
        clock_t timeTemp = clock();
        searchByID(&container, 750);
        average += (double)(clock() - timeTemp);
    }
    printf("%lf\n", (average/(1000000/**CLOCKS_PER_SEC*/)));
    while(container->head != NULL){
        removeByID(&container, 0);
    }
    free(container);
    return 0;
}

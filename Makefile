RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
all:
	gcc -o exc1.o exc1.c
	gcc -o exc2.o exc2.c
	gcc -o exc3.o exc3.c
	echo ${GREEN}Compilation finished!${NC}
	echo ${GREEN}Start First Program!${NC}
	./exc1.o
	echo ${GREEN}Start Second Program!${NC}
	./exc2.o
	echo ${GREEN}Start Third Program!${NC}
	./exc3.o